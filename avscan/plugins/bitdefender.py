"""
Implementation of the AVAdapter for the Bitdefender virus scanner
web site: http://www.bitdefender.com

Copyright (C) 2013  Lior Amar

This file is part of avscan module

avscan is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""



from avscan.av_adapter import IAVAdapter, ScannerThread, ScannerThreadStatusEnum
from avscan.core import AVInfo, AVResult, AVStatus, AvResultEnum, AVStatusEnum
from datetime import datetime

import re
import subprocess
import time


class Bitdefender(IAVAdapter):
    """
    Specific implementation of avadapter for bitdefender
    """
    my_name = "bitdefender"


    scan_summary_title = "Results:"

    def __init__(self):
        """
        Empty
        """
        self._line_parsed = 0
        self._cmd = "/opt/BitDefender-scanner/bin/bdscan"
        self._in_summary = False
        self._result = AVResult()
        self._verbose = False

    def verbose(self, v):
        self._verbose = v

    def info(self):
        """
        Gather information about bitdefender installation
        """
        info = AVInfo()
        info.name = self.my_name
        info.update_date = "12.12.12"
        info.scanner_cmd = self._cmd


# Getting the database age
        proc = subprocess.Popen([self._cmd, "--info"],
                                stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        return_code = proc.wait()

        # Read from pipes
        #print("Ret code: {0}".format(return_code))
        if return_code != 0:
           info.update_date = "Error"
           info.db_age = 1000000
        else:
            for line in proc.stdout:
                l = line.decode()
                l = l.strip()
                l = l.lstrip()
                if l.startswith("Update time GMT:"):
                    m = re.match(".*?:(.*)", l)
                    if m:
                        #Tue Dec 10 07:46:50 2013
                        fmt = '%a %b %d %H:%M:%S %Y'
                        d1 = datetime.strptime(m.group(1).lstrip(), fmt)
                        d2 = datetime.now()
                        info.db_age = (d2-d1).days
                        info.update_date = d1.strftime("%Y-%m-%d")
        return info

    def parse_summary_line(self, line):
        """
        Parsing a summary line of bdscan

        Results:
        Folders: 0
        Files: 3
        Packed: 0
        Archives: 0
        Infected files: 1
        Suspect files: 0
        Warnings: 0
        Identified viruses: 1
        I/O errors: 0
        Scan time: 00:00:12
        """
        parts = re.split(":\s*", line)
        data_parts = re.split("\s+", parts[1])

        if parts[0] == "Folders":
            self._result.scanned_dirs = int(data_parts[0])
        elif parts[0] == "Files":
            self._result.scanned_files = int(data_parts[0])
        elif parts[0] == "Infected files":
            self._result.infected_files = int(data_parts[0])
        elif parts[0] == "I/O errors":
            self._result.errors = int(data_parts[0])

    def parse_file_status_line(self, line):
        """
        Parsing line of the form:
        /tmp/ss/v  infected: EICAR-Test-File (not a virus)\n
        """
        if not line or len(line) == 0:
            return

        if line.startswith("Error:"):
            m = re.match("Error:\s+(.*):\s+(.*)", line)
            if m:
                self._result.error_list.append((m.group(2), m.group(1)))
            return

        parts = re.split("\s{2}", line)
        filename = parts[0]
        file_status = parts[1]

        print("Parsing file status: [{0}] --- [{1}]".format(filename, file_status))

        if file_status.startswith("infected:"):
            self._result.infected_list.append((filename, file_status))
            print("adding to infected")
        else:
            self._result.infected_list.append((filename, file_status))

    def handle_output_line(self, line):
        """
        Parse a line obtained from the output of bdscan
        """

        self._line_parsed = self._line_parsed + 1
        if self._verbose:
            print("bitdefender handle line:{0} : {1} [{2}]".format(self._line_parsed, len(line), line.strip()))

        if (not line) or len(line.strip()) == 0:
            return True

        # First 8 lines are general information lines
        if (self._line_parsed >= 1 and self._line_parsed <= 6) :
            return True

        # Empty lines are ignored
        if len(line) == 0:
            return True

        if line.startswith(self.scan_summary_title):
            self._in_summary = True
            return True
        elif self._in_summary:
            self.parse_summary_line(line)
        else:
            self.parse_file_status_line(line)
            pass

        return True

    def scan(self, path):
        # "strace", "-f", "-o", "/tmp/s",
        cmd = [ self._cmd, "--no-list", path]
        print("Running cmd [{0}]".format(cmd))
        self.thread = ScannerThread(cmd=cmd, line_parser=self.handle_output_line, pty=False, line_sep="\n")
        if self._verbose is True:
            self.thread.verbose = True

        self._start_time = time.time()
        self.thread.start()

    def status(self):
        s = AVStatus()
        thread_status = self.thread.get_status()
        s.status = ScannerThreadStatusEnum.thread_status_to_av_status(thread_status)
        s.msg = "parsed {0} lines".format(self._line_parsed)
        return s

    def result(self):
        """
        Return result object
        """

        result = self._result
        ts = self.thread.get_status()
        # Still running or failed we can not obtain results
        if ts is not ScannerThreadStatusEnum.ST_STATUS_DONE:
            result.status = None
        else:
            result.time = self.thread.end_time - self.thread.start_time
            if result.infected_files > 0:
                result.status = AvResultEnum.AV_RESULT_INFECTED
            elif result.errors > 0:
                result.status = AvResultEnum.AV_RESULT_ERROR
            else:
                result.status = AvResultEnum.AV_RESULT_OK

        return result
