"""
Implementation of the AVAdapter for the ClamAV virus scanner
web site: http://www.clamav.net

Copyright (C) 2013  Lior Amar

This file is part of avscan module

avscan is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from avscan.av_adapter import IAVAdapter, ScannerThread, ScannerThreadStatusEnum
from avscan.core import AVInfo, AVResult, AVStatus, AvResultEnum, AVStatusEnum

import re
import subprocess
import time

class ClamAV(IAVAdapter):
    """
    Specific implementation of avadapter for clamav
    """

    my_name = "clamav"

    scan_summary_title = "----------- SCAN SUMMARY -----------"

    def __init__(self):
        """
        Empty
        """
        self._line_parsed = 0
        self._clamscan = "/opt/clamav/bin/clamscan"
        self._get_update_date_cmd = "/home/lior/src/avscan/avscan/plugins/ttt"
        self._in_summary = False
        self._start_time = None
        self._end_time = None
        self._verbose = False

        self._result = AVResult()

    def verbose(self, v):
        self._verbose = v

    def info(self):
        info = AVInfo()
        info.name = self.my_name
        info.update_date = "12.12.12"
        info.scanner_cmd = self._clamscan

        # getting update date
        proc = subprocess.Popen(["env", "LD_LIBRARY_PATH=/opt/clamav/lib", self._get_update_date_cmd],
                                stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        return_code = proc.wait()

        # Read from pipes
        #print("Ret code: {0}".format(return_code))
        if return_code != 0:
           info.update_date = "Error"
           info.db_age = 1000000
        else:
            first_line = proc.stdout.readline()
            first_line = first_line.decode()
            parts = first_line.split(",")
            info.update_date = parts[0]
            info.db_age = int(int(parts[1])/(60*60*24))

        return info

    def parse_summary_line(self, line):
        """
        Parsing a summary line of clamav
            Known viruses: 2945544
            Engine version: 0.98
            Scanned directories: 378
            Scanned files: 2094
            Infected files: 0
            Total errors: 52
            Data scanned: 9.67 MB
            Data read: 6.70 MB (ratio 1.44:1)
            Time: 17.831 sec (0 m 17 s)
        """
        parts = re.split(":\s*", line)
        #print("Parts: {0}".format(str(parts)))
        data_parts = re.split("\s+", parts[1])

        if parts[0] == "Scanned directories":
            self._result.scanned_dirs = int(data_parts[0])
        elif parts[0] == "Scanned files":
            self._result.scanned_files = int(data_parts[0])
        elif parts[0] == "Infected files":
            self._result.infected_files = int(data_parts[0])
        elif parts[0] == "Total errors":
            self._result.errors = int(data_parts[0])

    def parse_file_status_line(self, line):
        """
        Parsing line of the form: file-name: status-str
        """
        line = line.strip()
        parts = re.split(":\s+", line)
        filename = parts[0]
        file_status = parts[1]
        #print("got file status: [{0}] [{1}]".format(filename, file_status))
        if file_status == "Access denied":
            self._result.error_list.append((filename, file_status))
        elif file_status == "Symbolic link":
            pass
        else:
            self._result.infected_list.append((filename, file_status))

    def handle_output_line(self, line):
        if self._verbose:
            print("clamav: [{0}]".format(line.strip()))
        self._line_parsed = self._line_parsed + 1

        if not line or len(line.strip()) == 0:
            return True

        # 4 first lines may contain a warning
        if (self._line_parsed >= 1 and self._line_parsed <= 4) and line.startswith("LibClamAV Warning"):
            return True

        if line.startswith(self.scan_summary_title):
            self._in_summary = True
            return True
        elif self._in_summary:
            self.parse_summary_line(line)
        else:
            # Parsing a regular line: file: status
            self.parse_file_status_line(line)

        return True

    def scan(self, path):
        cmd = [self._clamscan, "-r", "--suppress-ok-results",  path]

        self.thread = ScannerThread(cmd=cmd, line_parser=self.handle_output_line, pty=False, line_sep="\n")
        if self._verbose is True:
            self.thread.verbose = True

        self._start_time = time.time()
        self.thread.start()


    def status(self):
        s = AVStatus()
        thread_status = self.thread.get_status()
        s.status = ScannerThreadStatusEnum.thread_status_to_av_status(thread_status)
        s.msg = "parsed {0} lines".format(self._line_parsed)
        return s

    def result(self):
        """
        Return result object
        """

        result = self._result
        ts = self.thread.get_status()
        # Still running or failed we can not obtain results
        if ts is not ScannerThreadStatusEnum.ST_STATUS_DONE:
            result.status = None
        else:
            result.time = self.thread.end_time - self.thread.start_time
            if result.infected_files > 0:
                result.status = AvResultEnum.AV_RESULT_INFECTED
            elif result.errors > 0:
                result.status = AvResultEnum.AV_RESULT_ERROR
            else:
                result.status = AvResultEnum.AV_RESULT_OK

        return result

