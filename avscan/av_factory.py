"""
A Factory of AV

Copyright (C) 2013  Lior Amar

This file is part of avscan module

avscan is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from avscan.av_adapter import IAVAdapter
from avscan.plugins.clamav import ClamAV
from avscan.plugins.fprot import FProt
from avscan.plugins.bitdefender import Bitdefender
from avscan.plugins.avg import AVG

class AVFactory:
    """
    Factory of AV
    """
    def get_adapter_list(self):
        """
        Return list of adapters name installed
        """
        l = []
        for cls in IAVAdapter.__subclasses__():
            l.append(cls.my_name)
        return l

    def get_adapter(self, name):
        #print "In factory:", name
        for cls in IAVAdapter.__subclasses__():
            if cls.av_adapter_for(name):
                return cls()
        raise ValueError
