"""
Copyright (C) 2013  Lior Amar

This file is part of avscan module

avscan is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class AVStatus():
    """
    Status of a single av scan
    """
    status = None
    msg = ""


class AVStatusEnum:
    """
    Holding possible statuses of av engine
    """
    AV_STATUS_INIT = "init"
    AV_STATUS_RUNNING = "running"
    AV_STATUS_DONE = "done"
    AV_STATUS_FAIL = "fail"


class AvResultEnum:
    """
    Holding possible statuses of av scan
    """
    AV_RESULT_FAIL = "fail"
    AV_RESULT_OK = "ok"
    AV_RESULT_ERROR = "error"
    AV_RESULT_INFECTED = "infected"


class AVInfo():
    """
    General information about the av
    """
    name = None
    update_date = None
    db_age = None
    scanner_cmd = None

class AVResult():
    """
    Result of a single av scan
    """

    def __init__(self):
        self.status = None
        self.result = None
        self.scanned_files = 0
        self.scanned_dirs = 0
        self.infected_files = 0
        self.errors = 0
        self.time = 0.0

        self.infected_list = []
        self.error_list = []

    def __str__(self):
        s = ""
        s += "Results of scan:\n"
        s += "Status:         {0}\n".format(self.status)
        s += "scanned files:  {0}\n".format(self.scanned_files)
        s += "scanned dirs:   {0}\n".format(self.scanned_dirs)
        s += "infected files: {0}\n".format(self.infected_files)
        s += "errors:         {0}\n".format(self.errors)
        s += "time:           {0:.0f}\n".format(self.time)
        return s




class AVScanStatus():
    """
    Status of multiple av scan
    """

class AVScanResult():
    """
    Result of multiple av scan
    """
