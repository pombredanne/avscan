"""
av-scanner.py performs scan of a directory with multiple anti virus scanners

Copyright (C) 2013  Lior Amar

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import re
import argparse
import time

from avscan.core import AVInfo, AVStatusEnum, AvResultEnum
#import avscan.av_adapter

from avscan.av_factory import AVFactory

#print("Hello world")



description_help = """
av-scanner - a general anti virus scanner interface and cmdline for
             multiple av scanners on Linux

Usages:

    > av-scanner.py --av clamav,bitdefender --scan /etc

    Run a scan using 2 av: Clamav and Bitdefender

"""

authors_help = """
Authors: Lior Amar, Gal Oren
"""

class ScannerAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):

        #print('%r %r %r' % (namespace, values, option_string))
        setattr(namespace, self.dest, values)

        action_name = re.sub("^-{1,2}", "", option_string, count=2)
        #print "QueryType: [{0}]".format(query_name)
        setattr(namespace, "action_type", action_name)


def parse_args():

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                     description=description_help, epilog=authors_help)

    #parser.add_argument("scan_dir", type=str, metavar="scan-dir",
    #                    help="Directory to scan")
    a_group = parser.add_mutually_exclusive_group(required = True)

    parser.add_argument("--av", dest='av_list', default=None,
                        help='List of av scanners to use (comma separated)')

    a_group.add_argument('--list', action=ScannerAction, nargs=0, metavar="",
                         help='Print a list of installed av scanners')

    a_group.add_argument('--info', action=ScannerAction, nargs=0, metavar="",
                         help='Print information about each installed scanner')

    a_group.add_argument('--scan', action=ScannerAction, dest='scan_dir',
                         help='Run a scan, expecting a directory as the final argument')

    parser.add_argument("-v", dest='verbose', action="store_true", default=False,
                        help='Run in verbose mode printing debug information')

    options = parser.parse_args()
    #if options.cp_command == None:
    #    print "Please specify type of copy program to use (see help using --help)"
    #    sys.exit(1)

    return options


def run_scan(options):

    av_factory = AVFactory()

    if options.av_list:
        av_list = options.av_list.split(",")
    else:
        av_list = av_factory.get_adapter_list()

    av_objs = []
    for av in av_list:
        obj = av_factory.get_adapter(av)
        if options.verbose is True:
            obj.verbose(True)
        av_objs.append(obj)

    d = options.scan_dir
    print("Scanning: {0}".format(d))
    for obj in av_objs:
        obj.scan(d)

    done = False
    done_av_num = 0

    while done is False:
        done_av_num = 0
        for obj in av_objs:
            s = obj.status()
            print("Master: {0} --> {1}".format(s.status, s.msg))
            if s.status is AVStatusEnum.AV_STATUS_DONE or s.status is AVStatusEnum.AV_STATUS_FAIL:
                done_av_num += 1
                if done_av_num == len(av_objs):
                    done = True
        time.sleep(1)

    for obj in av_objs:
        print("\n\n{0} results".format(obj.my_name))
        result = obj.result()
        print(result)
        if result.infected_files > 0:
            print("Infected:")
            for item in result.infected_list:
                print("{0:20} {1}".format(item[0], item[1]))

        if result.errors > 0:
            print("Errors:")
            for item in result.error_list:
                print("{0:20} {1}".format(item[0], item[1]))


def print_info(options):

    av_factory = AVFactory()

    if options.av_list:
        av_list = options.av_list.split(",")
    else:
        av_list = av_factory.get_adapter_list()

    for av in av_list:
        print("{0}:".format(av))
        avobj = av_factory.get_adapter(av)
        av_info = AVInfo()
        av_info = avobj.info()
        print("  Date:   {0}".format(av_info.update_date))
        print("  DB Age: {0}".format(av_info.db_age))
        print("  Cmd:    {0}".format(av_info.scanner_cmd))
        print("")


def main():

    av_factory = AVFactory()

    options = parse_args()
    if options.action_type == "list":
        av_list = av_factory.get_adapter_list()
        for av in av_list:
            print("{0}".format(av))
    elif options.action_type == "info":
        print_info(options)
    elif options.action_type == "scan":
        print("Running scan")
        run_scan(options)


if __name__ == "__main__":
    main()
